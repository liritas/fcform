import { Routes, RouterModule } from '@angular/router';
import { TestFormComponent } from '@app/test-form/test-form.component';
import { NgModule } from '@angular/core';
import { FormDataDocumentResolver } from '@app/shared/form-data-document-resolver';

const routes: Routes = [
    { path: '', redirectTo: '/form', pathMatch: 'full' },
    { path: 'form', component: TestFormComponent, resolve: { testFormData: FormDataDocumentResolver } }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: false })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
