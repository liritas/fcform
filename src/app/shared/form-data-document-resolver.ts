import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { FormDataService } from './form-data-service.service';
import { Observable, throwError } from 'rxjs';
import { TestFormDocument } from './test-form-document';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class FormDataDocumentResolver implements Resolve<TestFormDocument> {
    constructor(private formDataService: FormDataService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<TestFormDocument> {
        return this.formDataService.getDocument().pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('error :(', error);
                return throwError(error);
            })
        );
    }
}
