import * as moment from 'moment';

import { Cloneable } from './cloneable';

export interface TestFormDocumentDto {
    name: string;
    purpose: string;
    travelStart: Date;
    travelEnd: Date;
    days: number;
    allowancePerDay: number;
    allowanceTotal: number;
}

export class TestFormDocument implements Cloneable<TestFormDocument>, TestFormDocumentDto {
    name: string;
    purpose: string;
    private travelStartValue: Date;
    private travelEndValue: Date;
    days: number;
    private allowancePerDayValue: number;
    allowanceTotal: number;


    // Using getters and setters instead properties

    set travelStart(date: Date) {
        this.travelStartValue = date;
        this.recalculateDays();
    }

    get travelStart() {
        return this.travelStartValue;
    }

    set travelEnd(date: Date) {
        this.travelEndValue = date;
        this.recalculateDays();
    }
    get travelEnd() {
        return this.travelEndValue;
    }

    get allowancePerDay() {
        return this.allowancePerDayValue;
    }

    set allowancePerDay(val: number) {
        this.allowancePerDayValue = this.numberToMoney(val);
        this.recalculateTotal();
    }

    recalculateDays(): void {
        if (this.travelStart && this.travelEnd) {
            const startDate = moment(this.travelStart).startOf('day');
            const endDate = moment(this.travelEnd).startOf('day');
            const days = endDate.diff(startDate, 'days') + 1;
            this.days = days > 0 ? days : 0; // prevent values < 0
        } else {
            this.days = 1;
        }
        this.recalculateTotal();
    }

    // function for calculating allowanceTotal
    recalculateTotal(): void {
        this.allowanceTotal = this.numberToMoney(this.days * this.allowancePerDay);
    }

    clone(): TestFormDocument {
        const testFormDocumentClone = Object.assign(Object.create(this), this) as TestFormDocument;
        return testFormDocumentClone;
    }

    // function for working with money decimal format
    private numberToMoney(val) {
        const num = Number.parseFloat(val).toFixed(2);
        const parsedNum = parseFloat(num);
        return isNaN(parsedNum) ? 0 : parsedNum;
    }
}

export class TestFormDocumentMapper {
    static FromDto(dto: TestFormDocumentDto): TestFormDocument {
        return Object.assign(new TestFormDocument(), <TestFormDocument>{
            name: dto.name,
            purpose: dto.purpose,
            travelStart: dto.travelStart ? new Date(dto.travelStart) : null,
            travelEnd: dto.travelEnd ? new Date(dto.travelEnd) : null,
            days: dto.days,
            allowancePerDay: dto.allowancePerDay,
            allowanceTotal: dto.allowanceTotal
        });
    }
}
