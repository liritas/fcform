import { Injectable } from '@angular/core';
import {
    TestFormDocument,
    TestFormDocumentMapper,
    TestFormDocumentDto
} from './test-form-document';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class FormDataService {
    constructor(private http: HttpClient) { }

    getDocument(): Observable<TestFormDocument> {
        const query = '/assets/exampledocument.json';
        return this.http
            .get<TestFormDocumentDto>(query)
            .pipe(
                map((dto: TestFormDocumentDto) => TestFormDocumentMapper.FromDto(dto))
            );
    }

    saveDocument(testFormDocument: TestFormDocument): void {
        console.log('saving', testFormDocument);
    }
}
