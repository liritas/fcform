import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// PrimeNG
import { ButtonModule, CalendarModule, CheckboxModule } from 'primeng/primeng';

import { AppComponent } from './app.component';
import { TestFormComponent } from './test-form/test-form.component';
import { FormsModule } from '@angular/forms';
import { FormDataService } from './shared/form-data-service.service';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormDataDocumentResolver } from './shared/form-data-document-resolver';

@NgModule({
    declarations: [AppComponent, TestFormComponent],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,

        // PrimeNG
        CalendarModule,
        CheckboxModule,
        ButtonModule,
        CalendarModule
    ],
    providers: [
        FormDataService,
        FormDataDocumentResolver
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
