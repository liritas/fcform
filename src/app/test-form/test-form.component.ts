import { Component, OnInit } from '@angular/core';
import { FormDataService } from '@app/shared/form-data-service.service';
import { TestFormDocument } from '@app/shared/test-form-document';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-test-form',
    templateUrl: './test-form.component.html',
    styleUrls: ['./test-form.component.css']
})
export class TestFormComponent implements OnInit {

    testFormDocument: TestFormDocument;
    travelDates: Array<Date>;

    constructor(
        private formDataService: FormDataService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.testFormDocument = this.route.snapshot.data['testFormData'] as TestFormDocument;
        this.travelDates = [this.testFormDocument.travelStart, this.testFormDocument.travelEnd];
    }

    onSave() {
        this.validateForm();
        this.formDataService.saveDocument(this.testFormDocument);
    }

    // event listener for datepicker in range mode, cause 2 way binding is not working with arrays
    selectDate() {
        [this.testFormDocument.travelStart, this.testFormDocument.travelEnd] = this.travelDates;
    }

    // prevent case when travelStart date is null
    validateForm() {
        if (!this.testFormDocument.travelEnd) {
            this.testFormDocument.travelEnd = this.testFormDocument.travelStart;
        }
    }
}
